using System;
using System.Collections.Generic;
using System.Text;

public abstract class LightNode
{
    public abstract string OuterHTML { get; }
    public abstract string InnerHTML { get; }
}

public class LightTextNode : LightNode
{
    private string text;

    public LightTextNode(string text)
    {
        this.text = text;
    }

    public override string OuterHTML => text;
    public override string InnerHTML => text;
}

public class LightElementNode : LightNode
{
    private string tagName;
    private bool isBlock;
    private bool isSelfClosing;
    private List<string> classes;
    private List<LightNode> children;

    public LightElementNode(string tagName, bool isBlock, bool isSelfClosing)
    {
        this.tagName = tagName;
        this.isBlock = isBlock;
        this.isSelfClosing = isSelfClosing;
        this.classes = new List<string>();
        this.children = new List<LightNode>();
    }

    public void AddClass(string className)
    {
        classes.Add(className);
    }

    public void AddChild(LightNode child)
    {
        children.Add(child);
    }

    public override string OuterHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<{tagName}");
            if (classes.Count > 0)
            {
                sb.Append($" class=\"{string.Join(" ", classes)}\"");
            }
            sb.Append(">");
            if (!isSelfClosing)
            {
                sb.Append(InnerHTML);
                sb.Append($"</{tagName}>");
            }
            if (isBlock)
            {
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }

    public override string InnerHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            foreach (LightNode child in children)
            {
                sb.Append(child.OuterHTML);
            }
            return sb.ToString();
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        LightElementNode div = new LightElementNode("div", true, false);
        div.AddClass("container");
        div.AddChild(new LightTextNode("Hello, world!"));
        div.AddChild(new LightElementNode("br", true, true));
        div.AddChild(new LightTextNode("It`s LightHTML"));

        Console.WriteLine(div.OuterHTML);

        LightElementNode table = new LightElementNode("table", true, false);

        for (int i = 0; i < 3; i++)
        {
            LightElementNode row = new LightElementNode("tr", true, false);
            for (int j = 0; j < 3; j++)
            {
                LightElementNode cell = new LightElementNode("td", true, false);
                cell.AddChild(new LightTextNode($"Cell {i + 1}-{j + 1}"));
                row.AddChild(cell);
            }
            table.AddChild(row);
        }

        Console.WriteLine(table.OuterHTML);

        LightElementNode header = new LightElementNode("h1", true, false);
        header.AddClass("header");
        header.AddChild(new LightTextNode("Welcome to LightHTML!"));

        Console.WriteLine(header.OuterHTML);
    }
}
