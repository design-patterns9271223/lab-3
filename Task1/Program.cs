using System;
using System.IO;

public class Logger
{
    public void Log(string message)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public void Error(string message)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }

    public void Warn(string message)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}

public class FileWriter
{
    public void Write(string fileName, string message)
    {
        File.WriteAllText(fileName, message);
    }

    public void WriteLine(string fileName, string message)
    {
        File.AppendAllText(fileName, message + Environment.NewLine);
    }
}

public class FileLogger : Logger
{
    private FileWriter fileWriter = new FileWriter();
    private string logFileName = "log.txt";

    public void LogToFile(string message)
    {
        fileWriter.Write(logFileName, message);
    }

    public void ErrorToFile(string message)
    {
        fileWriter.WriteLine(logFileName, "[ERROR] " + message);
    }

    public void WarnToFile(string message)
    {
        fileWriter.WriteLine(logFileName, "[WARNING] " + message);
    }
}

class Program
{
    static void Main(string[] args)
    {
        FileLogger fileLogger = new FileLogger();

        fileLogger.Log("This is a log message");
        fileLogger.Error("This is an error message");
        fileLogger.Warn("This is a warning message");

        fileLogger.LogToFile("This is a log message");
        fileLogger.ErrorToFile("This is an error message");
        fileLogger.WarnToFile("This is a warning message");
    }
}
