using System;
using System.IO;
using System.Text.RegularExpressions;

public interface ITextReader
{
    char[][] ReadText(string filePath);
}

public class SmartTextReader : ITextReader
{
    public char[][] ReadText(string filePath)
    {
        string[] lines = File.ReadAllLines(filePath);
        char[][] text = new char[lines.Length][];
        for (int i = 0; i < lines.Length; i++)
        {
            text[i] = lines[i].ToCharArray();
        }
        return text;
    }
}

public class SmartTextChecker : ITextReader
{
    private SmartTextReader reader;

    public SmartTextChecker(SmartTextReader reader)
    {
        this.reader = reader;
    }

    public char[][] ReadText(string filePath)
    {
        Console.WriteLine($"Opening file {filePath}...");
        char[][] text = reader.ReadText(filePath);
        Console.WriteLine($"Successfully read file {filePath}.");
        Console.WriteLine($"File {filePath} contains {text.Length} lines and {GetTotalChars(text)} characters.");
        Console.WriteLine($"Closing file {filePath}...");
        return text;
    }

    private int GetTotalChars(char[][] text)
    {
        int total = 0;
        foreach (char[] line in text)
        {
            total += line.Length;
        }
        return total;
    }
}

public class SmartTextReaderLocker : ITextReader
{
    private SmartTextReader reader;
    private Regex regex;

    public SmartTextReaderLocker(SmartTextReader reader, string pattern)
    {
        this.reader = reader;
        this.regex = new Regex(pattern);
    }

    public char[][] ReadText(string filePath)
    {
        if (regex.IsMatch(filePath))
        {
            Console.WriteLine("Access denied!");
            return new char[0][];
        }
        else
        {
            return reader.ReadText(filePath);
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        SmartTextReader reader = new SmartTextReader();
        SmartTextChecker checker = new SmartTextChecker(reader);
        SmartTextReaderLocker locker = new SmartTextReaderLocker(reader, @"^C:\\Windows\\.*$");

        char[][] text = checker.ReadText("test.txt");
        PrintText(text);

        text = locker.ReadText("C:\\Windows\\system32\\drivers\\etc\\hosts");
        PrintText(text);
    }

    static void PrintText(char[][] text)
    {
        foreach (char[] line in text)
        {
            Console.WriteLine(line);
        }
    }
}
