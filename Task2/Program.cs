using System;

public abstract class Hero
{
    public abstract void Attack();
}

public class Warrior : Hero
{
    public override void Attack()
    {
        Console.WriteLine("Warrior attacks!");
    }
}

public class Mage : Hero
{
    public override void Attack()
    {
        Console.WriteLine("Mage casts a spell!");
    }
}

public class Paladin : Hero
{
    public override void Attack()
    {
        Console.WriteLine("Paladin smites the enemy!");
    }
}

public abstract class HeroDecorator : Hero
{
    protected Hero hero;

    public HeroDecorator(Hero hero)
    {
        this.hero = hero;
    }

    public override void Attack()
    {
        hero.Attack();
    }
}

public class Sword : HeroDecorator
{
    public Sword(Hero hero) : base(hero) { }

    public override void Attack()
    {
        base.Attack();
        Console.WriteLine("Sword attack!");
    }

    public void Sharpen()
    {
        Console.WriteLine("Sword is sharpened!");
    }
}

public class Armor : HeroDecorator
{
    public Armor(Hero hero) : base(hero) { }

    public override void Attack()
    {
        base.Attack();
        Console.WriteLine("Armor defense!");
    }

    public void Reinforce()
    {
        Console.WriteLine("Armor is reinforced!");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Hero warrior = new Warrior();
        Hero mage = new Mage();
        Hero paladin = new Paladin();

        warrior = new Sword(warrior);
        mage = new Armor(mage);
        paladin = new Sword(new Armor(paladin));

        if (warrior is Sword sword)
            sword.Sharpen();
        if (mage is Armor armor)
            armor.Reinforce();

        warrior.Attack();
        mage.Attack();
        paladin.Attack();
    }
}
