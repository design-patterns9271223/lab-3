using System;

public abstract class Shape
{
    protected IRender render;

    public Shape(IRender render)
    {
        this.render = render;
    }

    public abstract void Draw();

    public void Resize(double factor)
    {
        Console.WriteLine($"Resizing the shape by a factor of {factor}");
    }
}

public interface IRender
{
    void RenderShape(string shapeName);
}

public class VectorRender : IRender
{
    public void RenderShape(string shapeName)
    {
        Console.WriteLine($"Drawing {shapeName} as lines");
    }
}

public class RasterRender : IRender
{
    public void RenderShape(string shapeName)
    {
        Console.WriteLine($"Drawing {shapeName} as pixels");
    }
}

public class Circle : Shape
{
    public Circle(IRender render) : base(render) { }

    public override void Draw()
    {
        render.RenderShape("Circle");
    }
}

public class Square : Shape
{
    public Square(IRender render) : base(render) { }

    public override void Draw()
    {
        render.RenderShape("Square");
    }
}

public class Triangle : Shape
{
    public Triangle(IRender render) : base(render) { }

    public override void Draw()
    {
        render.RenderShape("Triangle");
    }
}

class Program
{
    static void Main(string[] args)
    {
        IRender vectorRender = new VectorRender();
        IRender rasterRender = new RasterRender();

        Shape vectorCircle = new Circle(vectorRender);
        Shape rasterSquare = new Square(rasterRender);
        Shape vectorTriangle = new Triangle(vectorRender);

        vectorCircle.Draw();
        rasterSquare.Draw();
        vectorTriangle.Draw();

        vectorCircle.Resize(2);
        rasterSquare.Resize(1.5);
        vectorTriangle.Resize(0.5);
    }
}
