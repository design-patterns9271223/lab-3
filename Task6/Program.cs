using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public abstract class LightNode
{
    public abstract string OuterHTML { get; }
}

public class LightTextNode : LightNode
{
    private string text;

    public LightTextNode(string text)
    {
        this.text = text;
    }

    public override string OuterHTML => text;
}

public class LightElementNode : LightNode
{
    private static Dictionary<string, string> tagNames = new Dictionary<string, string>();

    private string tagName;
    private List<LightNode> children;

    public LightElementNode(string tagName)
    {
        if (!tagNames.ContainsKey(tagName))
        {
            tagNames[tagName] = tagName;
        }
        this.tagName = tagNames[tagName];
        this.children = new List<LightNode>();
    }

    public void AddChild(LightNode child)
    {
        children.Add(child);
    }

    public override string OuterHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<{tagName}>");
            foreach (LightNode child in children)
            {
                sb.Append(child.OuterHTML);
            }
            sb.Append($"</{tagName}>\n");

            return sb.ToString();
        }
    }
}

public class LightHTMLBuilder
{
    public LightElementNode BuildFromTextFile(string filePath)
    {
        string[] lines = File.ReadAllLines(filePath);
        LightElementNode root = new LightElementNode("div");
        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i];
            LightElementNode node;
            if (i == 0)
            {
                node = new LightElementNode("h1");
            }
            else if (line.Length < 20)
            {
                node = new LightElementNode("h2");
            }
            else if (line.StartsWith(" "))
            {
                node = new LightElementNode("blockquote");
            }
            else
            {
                node = new LightElementNode("p");
            }
            node.AddChild(new LightTextNode(line));
            root.AddChild(node);
        }
        return root;
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;

        LightHTMLBuilder builder = new LightHTMLBuilder();
        LightElementNode root = builder.BuildFromTextFile("gfe34.txt");

        long startMem = GC.GetTotalMemory(true);
        string html = root.OuterHTML;
        long endMem = GC.GetTotalMemory(true);

        Console.WriteLine(html);
        Console.WriteLine($"Approximate size of the markup tree in the process memory: {endMem - startMem} bytes");
        Console.ReadLine();
    }
}
